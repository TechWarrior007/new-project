2
Jump to Comments
20
Save

Cover image for Build and deploy React using Gitlab CI pipeline
AT Digitals profile imageTamilarasu S
Tamilarasu S for AT Digitals
Posted on 7 Jul 2021

 10  4  
Build and deploy React using Gitlab CI pipeline
#
gitlab
#
react
#
devops
Gitlab CI/CD (2 Part Series)
1
Build and deploy React using Gitlab CI pipeline
2
Deploy Node.js using Gitlab CI pipeline
Prerequisite
Repository in Gitlab with react project
Production server for which you have SSH access
NGINX setup for your domain to a folder in the server. See this tutorial on how to do it
Goal
Setup the Gitlab CI/CD pipeline to create a react production build and deploy to our server whenever code is merged to master branch. The attached steps can also be used for other SPA like Angular, Vue.js.

Step 1 - Give Gitlab access to your server
We are going to use Gitlab CI/CD Variables to save a private SSH key which Gitlab will use to authenticate with the server.

We are going to use SSH keys to authenticate rather than username and password as it is more secure.

This can be configured at a repository level or at a group level.
To view all the CI/CD variables of your repository,

Go to your project’s Settings > CI/CD
Expand the Variables section.
You can also view the same at a group level by first navigating to the group (Menu > Groups > Your Groups) and following the same steps.

If you already have a SSH_PRIVATE_KEY private key variable listed, you can skip this step.

To create a new variable, Select the Add Variable button and fill in the details:

Key: SSH_PRIVATE_KEY
Value: <ssh_private_key_details>. (To generate a new SSH public and private key pair, follow steps from this guide. Make sure to not accidentally overwrite any existing key pairs.)
Type: Variable
Choose other settings based on your needs
Click Add Variable to add the variable

Gitlab CI/CD Variable Config

Step 2 - Add Gitlab SSH public key to your server
Now, we need to add SSH public key to the list of authorized_keys in the production server.

SSH into your server (ssh root@example.com)
Add the SSH public key to authorized_keys
nano ~/.ssh/authorized_keys
Paste the SSH public key(starts withssh-rsa) in a new line

Save the file

Step 3 - Configuring Gitlab CI/CD
Gitlab looks for .gitlab-ci.yml in the root folder of your repository for CI/CD pipeline configurations

Add a new file .gitlab-ci.yml in the root folder
image: node

cache:
  paths:
    - node_modules/

before_script:
  - apt-get update -qq
  - apt-get install -qq git
  - "which ssh-agent || ( apt-get install -qq openssh-client )"
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

deploy:
  stage: deploy
  environment:
    name: production
    url: example.com
  script:
    - bash deploy/deploy.sh
  only:
    - master
Update the url in the above file to your domain
Explanation
We are using the Node docker image as the starting point
We are caching the node_modules folder to improve the speed of the build
We install git package and then configure it to add our SSH_PRIVATE_KEY
We also configure StrictHostKeyChecking to no, to ensure git doesn't show manual prompt during initial connection.
We have setup pipeline named deploy with a single pipeline stage deploy which listens to commits on master and runs the script in deploy/deploy.sh
Step 4 - Setup the deploy script
Add a new file deploy.sh in deploy folder
#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="html-folder-in-server"

# Building React output
yarn install
yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ root@${DEPLOY_SERVER}:/var/www/html/${SERVER_FOLDER}/

echo "Finished copying the build files"
Update the server folder to the folder name that you have created in the production server
Set the CI/CD variable DEPLOY_SERVER with value domain.com for the repository using step 1.
Explanation
We set server and folder variables
We install dependencies and then start a new react production build using yarn
Once the build is done, we copy the build folder to /var/www/html/html-folder-in-server/build location in server
Step 5 - Setup a Gitlab runner (One time setup)
We need a runner to run our CI/CD pipeline jobs. This step is optional if a runner is already configured for your group in Gitlab.

To setup a new Gitlab group runner

Install Gitlab Runner in any server with atleast 2GB of RAM using the steps from the documentation. The server should be separate from where Gitlab is installed for security reasons.
Go to your group's Settings > CI/CD
Expand the Runners section.
Under the Set up a group runner manually section, copy the url and token
Then register the runner in your server using steps from documentation
Provide default image as ubuntu and empty tags
Once the runner is registered, go back to the Runners section in Gitlab to see the runner appear under Available runners section
Push the .gitlab-ci.yml and deploy/deploy.sh files to master to start the automated deployment.

When the deployment is complete, you should see a green checkmark in the repository home page similar to
Pipeline Status Image

You can click the status icon to go to the pipeline and then to the individual job to see the command line output of your script

Troubleshooting
If the job is not scheduled, make sure you have setup a runner and that the runner is active. Also check the runner parameters like active, protected and tags to see if any of the conditions are incorrect.
If the job fails to produce a build output due to memory allocation issue, try increasing the memory of the server which hosts the runner.
References
https://medium.com/devops-with-valentine/deploy-over-ssh-from-gitlab-ci-pipeline-6a0d7b87e4a
https://medium.com/@hfally/a-gitlab-ci-config-to-deploy-to-your-server-via-ssh-43bf3cf93775
https://codeburst.io/gitlab-build-and-push-to-a-server-via-ssh-6d27ca1bf7b4
https://erangad.medium.com/create-ci-cd-pipeline-for-nodejs-express-with-gitlab-6c420a956b10
Gitlab CI/CD (2 Part Series)
1
Build and deploy React using Gitlab CI pipeline
2
Deploy Node.js using Gitlab CI pipeline
Top comments (2)

Subscribe
pic
Add to the discussion
 
 
ezalivadnyi profile image
Eugene Zalivadnyi
•
14 Aug 22

What can happen if we will use the same server for gitalb-runner and production?


1
 like
Like
Reply
 
 
xpcrts profile image
Phok Chanrithisak
•
11 Nov 22 • Edited on 11 Nov

Where & how to see the result? Is it using server ip address follow with port 3000?


Like
Reply
Code of Conduct • Report abuse
DEV Community

We are hiring! Do you want to be our Senior Platform Engineer?Forem is hiring a Senior Platform Engineer

If you're interested in ops and site reliability and capable of dipping in to our Linux stack, we'd love your help shoring up our systems!

Read next
pavanbelagatti profile image
Kubernetes Deployments: Rolling vs Canary vs Blue-Green
Pavan Belagatti - Feb 21

tommyhenchuanlin profile image
Pipeline example using Terraform, Jenkins, Ansible, and Docker on Google Cloud-Part 1
Tommy Heng-Chuan Lin - Feb 25

seeyouoncloud profile image
Protecting Your Sensitive Data in the Cloud with Amazon S3 Encryption
Amruta Pardeshi - Feb 25

dellamora profile image
Criei um hook personalizado para buscar dados da API do Rick and Morty com React Query e TypeScript
Francielle Dellamora - Feb 23


AT Digitals
Follow
Design Develop Innovate
If you need a new website to develop at an affordable price, send us a message with your request.
We help early stage startup and new small and medium enterprises with their web development needs.

Contact Us
More from AT Digitals
Deploy Node.js using Gitlab CI pipeline
#node #devops #gitlab
DEV Community

🌚 Life is too short to browse without dark mode
You can customize your theme, font, and more when you are signed in.

#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="html-folder-in-server"

# Building React output
yarn install
yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ root@${DEPLOY_SERVER}:/var/www/html/${SERVER_FOLDER}/

echo "Finished copying the build files"